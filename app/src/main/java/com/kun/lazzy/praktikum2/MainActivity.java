package com.kun.lazzy.praktikum2;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static android.widget.Toast.*;

public class MainActivity extends AppCompatActivity {

    private int nCount= 0;
    private TextView mShowCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mShowCount = (TextView) findViewById(R.id.show_count);

    }

    public void countUp(View view) {
        nCount++;
        if (mShowCount != null)
            mShowCount.setText(String.valueOf(nCount));
    }

    public void showToast(View view) {
        Context context = getApplicationContext();
        Toast toast =Toast.makeText(context, "Hello Toast", LENGTH_LONG);
        toast.show();
    }


    public void resetToast(View view) {
        nCount = 0;
      if (mShowCount != null)
          mShowCount.setText(String.valueOf(nCount));
        }
    }

